
#
# 1. State College Temperatura Data
#

data <- read.csv("./data/DailyTempSCE20022016.csv")

#
# A) Create our aggregated data for month, year, and average temperature
#

# First we get data out
data_months <- data.frame(
    year = substr(data$DATE, 1, 4),
    month = substr(data$DATE, 5, 6),
    daily_mean_temp = (data$TMAX - data$TMIN) / 2
)
str(data_months)

# NOTE: There are some abnormal averages (e.g. -5021.5 or 5009). Since the scale
# seems to be in C degrees, I'll remove those extreme values, otherwise the plot
# looks very bad. I found this after creating the plot the first time, then I
# looked for the max and min values to know where I should cut the data.

data_months <- data_months[data_months$daily_mean_temp <  1000, ]
data_months <- data_months[data_months$daily_mean_temp > -1000, ]

# NOTE: Now we aggregate using the `aggregate()` function in R. I'm leaving this
# for your reference here. Below I show how to do it using a for-loop approach.
#
# data_months_agg_other <- aggregate(
#     data_months$daily_mean_temp,
#     by = list(
#         "Month" = data_months$month,
#         "Year" = data_months$year
#     ),
#     FUN = mean,
#     na.rm = TRUE
# )
# names(data_months_agg_other) <- c("Month", "Year", "MeanTemp")
# str(data_months_agg_other)

# Now we aggregate using a for-loop
data_months_agg <- data.frame(
    Month = NULL,
    Year = NULL,
    MeanTemp = NULL
)
for (year in unique(data_months$year)) {
    print(year)
    for (month in unique(data_months$month)) {
        print(paste("  ", month))
        month_mean <- mean(data_months[
            data_months$year == year &
            data_months$month == month,
            "daily_mean_temp"
        ])
        if (!is.nan(month_mean)) {
            # If it were NaN, the year-month combination does
            # not exist in the data (e.g. 2017-02 ... 2017-12),
            # and we should not add it in that case.
            data_months_agg <- rbind(
                data_months_agg,
                data.frame(
                    Month = month,
                    Year = year,
                    MeanTemp = month_mean
                )
            )
        }
    }
}
str(data_months_agg)

# NOTE: To check that both approaches give the same results, you may use the
# following code. In the for-loop you can see value by value compared, and with
# the `identical()` function you can check that all entries are the same in a
# single command.
#
# for (year in unique(data_months$year)) {
#     print(year)
#     for (month in unique(data_months$month)) {
#         print(paste(" ", month))
#         mean_1 <- data_months_agg[
#             data_months_agg$Year == year &
#             data_months_agg$Month == month,
#             "MeanTemp"
#         ]
#         mean_2 <- data_months_agg_other[
#             data_months_agg_other$Year == year &
#             data_months_agg_other$Month == month,
#             "MeanTemp"
#         ]
#         print(paste("  ", mean_1))
#         print(paste("  ", mean_2))
#     }
# }
#
# identical(data_months_agg, data_months_agg_other)

#
# B) Plot average monthly temperature for all years
#

# NOTE: Understanding the `x` specification in the `aes()` function may be a bit
# tricky. First we put year and month together into a single string using the
# `paste()` function, then we convert those into factors (to get an integer
# assigned to them in increasing order), and then we extract that integer,
# instead of the "2002/01" labels, with the `as.integer()` function. Also, you
# can adjust the breaks in the x-axis ticks changing the corresponding sequence
# (seq(...)).

library(ggplot2)

plot <- ggplot(
    data_months_agg,
    aes_string(
        x = as.integer(as.factor(paste(
            data_months_agg$Year, "/",
            data_months_agg$Month, sep = ""
        ))),
        y = "MeanTemp",
        group = 1
    )
)
plot <- plot + geom_point() + geom_line()
plot <- plot + scale_x_continuous(breaks = seq(1, 181, 2))
print(plot)

# C) Repeat A) and B) for seasons

# NOTE: The approach is the same, but what changes is the way we get the season
# data. In this case I use a function that receives a date (e.g. '20020101'),
# extracts the month, and and checks what season it should get. We call such
# function for each observation in the data.

season <- function(date) {
    month <- as.integer(substr(date, 5, 6))
    if (month %in% c(3, 4, 5)) {
        return('Spring')
    } else if (month %in% c(6, 7, 8)) {
        return('Summer')
    } else if (month %in% c(9, 10, 11)) {
        return('Fall')
    } else if (month %in% c(12, 1, 2)) {
        return('Winter')
    } else {
        stop('Error in getting season')
    }
}

data_seasons <- data.frame(
    year = substr(data$DATE, 1, 4),
    season = unlist(lapply(data$DATE, season)),
    daily_mean_temp = (data$TMAX - data$TMIN) / 2
)
str(data_seasons)
head(data_seasons)

data_seasons <- data_seasons[data_seasons$daily_mean_temp <  1000, ]
data_seasons <- data_seasons[data_seasons$daily_mean_temp > -1000, ]

data_seasons_agg <- aggregate(
    data_seasons$daily_mean_temp,
    by = list(
        "season" = data_seasons$season,
        "year" = data_seasons$year
    ),
    FUN = mean,
    na.rm = TRUE
)
names(data_seasons_agg) <- c("Season", "Year", "MeanTemp")
str(data_seasons_agg)

plot <- ggplot(
    data_seasons_agg,
    aes_string(
        x = as.integer(as.factor(paste(
            data_seasons_agg$Year, "/",
            data_seasons_agg$Season, sep = ""
        ))),
        y = "MeanTemp",
        group = 1
    )
)
plot <- plot + geom_point() + geom_line()
plot <- plot + scale_x_continuous(breaks = seq(1, 181, 2))
print(plot)

#
# 2. Twitter Data
#

load(url("http://personal.psu.edu/muh10/380/data/testTweets.rda"))

#
# Initial question
#

str(testTweets)

# Answer: The data frame has 3 variables (`Ttext`, `Tdate`, and `Tdev`), all of
# which are character (string) variables, and it has 5 observations (tweets).

#
# Retweets
#

RT <- grepl("^RT", testTweets$Ttext)

#
# Tweet hours
#

# Unvectorized
hr <- unlist(lapply(testTweets$Tdate, function(timestamp) {
    r <- regexpr("[0-9]{2}:", timestamp)
    print(substr(timestamp, r, r + attr(r, "match.length") - 2))
    return(substr(timestamp, r, r + attr(r, "match.length") - 2))
}))

# Vectorized
hr <- substr(
    testTweets$Tdate,
    regexpr("[0-9]{2}:", testTweets$Tdate),
    (regexpr("[0-9]{2}:", testTweets$Tdate) +
     attr(regexpr("[0-9]{2}:", testTweets$Tdate), "match.length") - 2))

#
# Hash Tags
#

hashtag_on_its_own <- grepl(" # ", testTweets$Ttext)
anyHash <- grepl("#", testTweets$Ttext) & !hashtag_on_its_own

#
# NOTE: Note the following is not for your homework, but for you to understand
# the check. Note the difference with and without the check, and you should see
# the difference.
#
# tweets <- testTweets$Ttext
# tweets <- c(tweets, "Tweet with a single # hashtag")
#
# anyHash <- grepl("#", tweets)
#
# hashtag_on_its_own <- grepl(" # ", tweets)
# anyHash <- grepl("#", tweets) & !hashtag_on_its_own

#
# Extract hashtags
#

hashLocs <- gregexpr(pattern = "#[[:alpha:]]+", testTweets$Ttext)

hashLocs[[1]]
hashLocs[[5]]
hashLocs[[3]]

hashtagList <-
  mapply(function(x, sta) {
    substr(rep(x, length(sta)),
           start = sta,
           stop = sta + attributes(sta)$match.length - 1)
  },
         x = testTweets$Ttext,
         sta = hashLocs)
names(hashtagList) = NULL
hashtagList

#
# 3. Reading in real Twitter data
#

library(RJSONIO)
tweetLines <- readLines(url("http://personal.psu.edu/muh10/380/data/realDonaldTrump.json"))
tweetList <- fromJSON(paste(tweetLines, collapse = " "))
names(tweetList) = NULL

# NOTE: You may use a tool like JSON Formatter (https://jsonformatter.org/) to
# understand the structure behind the JSON; I find it easier than using R to
# explore the structure itself. In this case we have a list of dictionaries
# (key-value objects). As you can see, you have a `text`, `created_at`, and
# `source` fields in each dictionary contain the text, time, and device
# information. You may also find them by position.

# To explore the structure within R, you may use the following command:
str(tweetList)

tweetList[[1]]$text
tweetList[[1]][[5]]

tweetList[[1]]$created_at
tweetList[[1]][[7]]

tweetList[[1]]$source
tweetList[[1]][[10]]

# Extract the tweets into a character vector:
tweets <- unlist(lapply(tweetList, function(object) { return(object$text) }))
